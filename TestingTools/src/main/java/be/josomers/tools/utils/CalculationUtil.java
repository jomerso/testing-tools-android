package be.josomers.tools.utils;

/**
 * @author Jo Somers
 */
public class CalculationUtil {

    /**
     * Calculate the sum of 2 parameters
     *
     * @param firstNumber  {@link java.lang.Integer}
     * @param secondNumber {@link java.lang.Integer}
     * @return sum of 2 {@link java.lang.Integer}
     */
    public static int sum(int firstNumber, int secondNumber) {
        return firstNumber + secondNumber;
    }

}

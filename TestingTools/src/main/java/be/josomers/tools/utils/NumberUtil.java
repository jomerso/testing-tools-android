package be.josomers.tools.utils;

/**
 * @author Jo Somers
 */
public class NumberUtil {

    /**
     * Parse a given {@link java.lang.String} to an {@link java.lang.Integer} value
     *
     * @param input {@link java.lang.String}
     * @return {@link java.lang.Integer}
     */
    public static int parseStringToInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}

package be.josomers.tools.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import be.josomers.tools.R;

import static be.josomers.tools.utils.CalculationUtil.sum;
import static be.josomers.tools.utils.NumberUtil.parseStringToInt;

/**
 * @author Jo Somers
 */
public class MainFragment extends Fragment {

    private Button calculateSumButton;
    private Button resetResultButton;
    private EditText firstNumberEditText;
    private EditText secondNumberEditText;
    private TextView resultTextView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon
     * screen orientation changes).
     */
    public MainFragment() {
    }

    @Override
    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        calculateSumButton = (Button) rootView.findViewById(R.id.calculateSum);
        resetResultButton = (Button) rootView.findViewById(R.id.resetResult);
        firstNumberEditText = (EditText) rootView.findViewById(R.id.firstNumber);
        secondNumberEditText = (EditText) rootView.findViewById(R.id.secondNumber);
        resultTextView = (TextView) rootView.findViewById(R.id.result);

        initListeners();

        return rootView;
    }

    private void initListeners() {
        calculateSumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCalculateSum();
            }
        });

        resetResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResetResult();
            }
        });
    }

    private void onCalculateSum() {
        final int firstNumber = parseStringToInt(firstNumberEditText.getText().toString());
        final int secondNumber = parseStringToInt(secondNumberEditText.getText().toString());
        final int result = sum(firstNumber, secondNumber);
        resultTextView.setText("" + result);
    }

    private void onResetResult() {
        resultTextView.setText("");
        firstNumberEditText.setText("");
        secondNumberEditText.setText("");
    }

}

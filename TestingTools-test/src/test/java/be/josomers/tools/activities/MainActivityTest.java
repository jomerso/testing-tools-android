package be.josomers.tools.activities;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import be.josomers.tools.R;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * @author Jo Somers
 */
@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

    private MainActivity mainActivity;

    private EditText firstNumberEditText;
    private EditText secondNumberEditText;
    private TextView resultTextView;
    private Button resetButton;
    private Button calculateSumButton;

    @Before
    public void setUp() {
        mainActivity = Robolectric.buildActivity(MainActivity.class).create().get();
        firstNumberEditText = (EditText) mainActivity.findViewById(R.id.firstNumber);
        secondNumberEditText = (EditText) mainActivity.findViewById(R.id.secondNumber);
        resultTextView = (TextView) mainActivity.findViewById(R.id.result);
        resetButton = (Button) mainActivity.findViewById(R.id.resetResult);
        calculateSumButton = (Button) mainActivity.findViewById(R.id.calculateSum);
    }

    @Test
    public void initFieldsTest() {
        assertThat(mainActivity).isNotNull();
        assertThat(firstNumberEditText).isNotNull();
        assertThat(secondNumberEditText).isNotNull();
        assertThat(resultTextView).isNotNull();
        assertThat(resetButton).isNotNull();
        assertThat(calculateSumButton).isNotNull();

        assertThat(firstNumberEditText.getText().toString()).isEmpty();
        assertThat(secondNumberEditText.getText().toString()).isEmpty();
        assertThat(resultTextView.getText().toString()).isEmpty();

        assertThat(resetButton.getText().toString()).isEqualToIgnoringCase("Reset the result");
        assertThat(calculateSumButton.getText().toString()).isEqualToIgnoringCase("Calculate sum");
    }

    @Test
    public void calculateSumTest() {
        preFillFieldsAndClickCalculate();

        assertThat(resultTextView.getText().toString()).isEqualTo("8");
    }

    @Test
    public void resetResultTest() {
        preFillFieldsAndClickCalculate();

        resetButton.performClick();

        assertThat(firstNumberEditText.getText().toString()).isEmpty();
        assertThat(secondNumberEditText.getText().toString()).isEmpty();
        assertThat(resultTextView.getText().toString()).isEmpty();
    }

    private void preFillFieldsAndClickCalculate() {
        firstNumberEditText.setText("6");
        secondNumberEditText.setText("2");
        calculateSumButton.performClick();
    }

}

package be.josomers.tools.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * @author Jo Somers
 */
@RunWith(RobolectricTestRunner.class)
public class CalculationUtilTest {

    @Test
    public void testInit() {
        final CalculationUtil calculationUtil = new CalculationUtil();
        assertThat(calculationUtil).isNotNull();
    }

    @Test
    public void testSum() {
        assertThat(CalculationUtil.sum(3, 7)).isEqualTo(10);
    }

}

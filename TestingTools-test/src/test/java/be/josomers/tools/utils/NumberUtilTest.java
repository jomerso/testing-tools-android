package be.josomers.tools.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static be.josomers.tools.utils.NumberUtil.parseStringToInt;
import static org.fest.assertions.api.Assertions.assertThat;

/**
 * @author Jo Somers
 */
@RunWith(RobolectricTestRunner.class)
public class NumberUtilTest {

    @Test
    public void testInit() {
        final NumberUtil numberUtil = new NumberUtil();
        assertThat(numberUtil).isNotNull();
    }

    @Test
    public void testParseStringIntValueToInt() {
        assertThat(parseStringToInt("2")).isEqualTo(2);
    }

    @Test
    public void testParseEmptyStringValueToInt() {
        assertThat(parseStringToInt("")).isEqualTo(0);
    }

    @Test
    public void testParseStringValueToInt() {
        assertThat(parseStringToInt("John")).isEqualTo(0);
    }

}
